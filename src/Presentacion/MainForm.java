package Presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import Logica.Juego;
import javax.swing.border.MatteBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainForm {
	
	private Juego juego;
	private boolean empezoJuego=false;
	private boolean seGuardoPuntaje=false;
	private boolean hubo2048=false;

	private JFrame frame;
	
	private JLabel puntaje;
	private JLabel lNombreJugador;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setTitle("2048");
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 20));
		frame.setBounds(600, 200, 711, 608);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setFocusable(true);

		
		
		JButton btnJuegoNuevo = new JButton("Juego nuevo");
		btnJuegoNuevo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_LEFT)
				{
					if(empezoJuego==true)
					{
						juego.mover("izquierda");
						dibujarMatriz();
						puntaje.setText(juego.getPuntaje().toString());
						juego.verificarSiterminoJuego();
						juego.verificarSiHay2048();
						guardarPuntaje();
					}
				}
				else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
				{
					if(empezoJuego==true)
					{
						juego.mover("derecha");
						dibujarMatriz();
						puntaje.setText(juego.getPuntaje().toString());
						juego.verificarSiterminoJuego();
						juego.verificarSiHay2048();
						guardarPuntaje();
					}
				}
				else if(e.getKeyCode()==KeyEvent.VK_UP)
				{
					if(empezoJuego==true)
					{
						juego.mover("arriba");
						dibujarMatriz();
						puntaje.setText(juego.getPuntaje().toString());
						juego.verificarSiterminoJuego();
						juego.verificarSiHay2048();
						guardarPuntaje();
					}
				}
				
				else if(e.getKeyCode()==KeyEvent.VK_DOWN)
				{
					if(empezoJuego==true)
					{
						juego.mover("abajo");
						dibujarMatriz();
						puntaje.setText(juego.getPuntaje().toString());
						juego.verificarSiterminoJuego();
						juego.verificarSiHay2048();
						guardarPuntaje();
					}
				}
			}
		});
		btnJuegoNuevo.setForeground(Color.WHITE);
		btnJuegoNuevo.setBackground(Color.BLACK);
		btnJuegoNuevo.setBounds(424, 18, 132, 23);
		
		btnJuegoNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				juego=new Juego();
				String nombre=JOptionPane.showInputDialog("ingrese su nombre");
				if(nombre.equals("")==false)
				{
					lNombreJugador.setText(nombre);
				}
				dibujarMatriz();
				puntaje.setText("0");
				dibujarMatriz();
				empezoJuego=true;
				seGuardoPuntaje=false;
				hubo2048=false;
				
			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(btnJuegoNuevo);
		
		JButton btnArriba = new JButton("Arriba");
		btnArriba.setForeground(Color.WHITE);
		btnArriba.setBackground(Color.BLACK);
		btnArriba.setBounds(290, 448, 103, 31);
		btnArriba.setFocusable(false);
		btnArriba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(empezoJuego==true)
				{
					juego.mover("arriba");
					dibujarMatriz();
					puntaje.setText(juego.getPuntaje().toString());
					juego.verificarSiterminoJuego();
					juego.verificarSiHay2048();
					guardarPuntaje();
				}
			}
		});
		frame.getContentPane().add(btnArriba);
		
		JButton btnIzq = new JButton("Izquierda");
		btnIzq.setForeground(Color.WHITE);
		btnIzq.setBackground(Color.BLACK);
		btnIzq.setBounds(253, 480, 90, 31);
		btnIzq.setFocusable(false);
		btnIzq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(empezoJuego==true)
				{
					juego.mover("izquierda");
					dibujarMatriz();
					puntaje.setText(juego.getPuntaje().toString());
					juego.verificarSiterminoJuego();
					juego.verificarSiHay2048();
					guardarPuntaje();
				}
			}
		});
		frame.getContentPane().add(btnIzq);
		
		JButton btnDer = new JButton("Derecha");
		btnDer.setForeground(Color.WHITE);
		btnDer.setBackground(Color.BLACK);
		btnDer.setBounds(347, 480, 90, 31);
		btnDer.setFocusable(false);
		btnDer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(empezoJuego==true)
				{
					juego.mover("derecha");
					dibujarMatriz();
					puntaje.setText(juego.getPuntaje().toString());
					juego.verificarSiterminoJuego();
					juego.verificarSiHay2048();
					guardarPuntaje();
				}
				
			}
		});
		frame.getContentPane().add(btnDer);
		
		JButton btnAbajo = new JButton("Abajo");
		btnAbajo.setForeground(Color.WHITE);
		btnAbajo.setBackground(Color.BLACK);
		btnAbajo.setBounds(290, 513, 103, 31);
		btnAbajo.setFocusable(false);
		btnAbajo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(empezoJuego==true)
				{
					juego.mover("abajo");
					dibujarMatriz();
					puntaje.setText(juego.getPuntaje().toString());
					juego.verificarSiterminoJuego();
					juego.verificarSiHay2048();
					guardarPuntaje();
				}
				
			}
		});
		frame.getContentPane().add(btnAbajo);
		
		JLabel lblNewLabel = new JLabel("Puntaje:");
		lblNewLabel.setBounds(10, 62, 110, 31);
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setFont(new Font("Segoe Print", Font.PLAIN, 22));
		frame.getContentPane().add(lblNewLabel);
		
		puntaje = new JLabel("0");
		puntaje.setBounds(130, 63, 162, 28);
		puntaje.setForeground(Color.YELLOW);
		puntaje.setFont(new Font("Segoe Print", Font.PLAIN, 22));
		frame.getContentPane().add(puntaje);
		
		JButton btnVerPuntajes = new JButton("Ver puntajes");
		btnVerPuntajes.setForeground(Color.WHITE);
		btnVerPuntajes.setBackground(Color.BLACK);
		btnVerPuntajes.setBounds(424, 52, 132, 23);
		btnVerPuntajes.setFocusable(false);
		btnVerPuntajes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Puntajes p=new Puntajes();
				p.mostrarPuntajes();
			}
		});
		frame.getContentPane().add(btnVerPuntajes);
		
		JLabel lblNombre = new JLabel("Nombre: ");
		lblNombre.setBounds(10, 18, 118, 31);
		lblNombre.setForeground(Color.CYAN);
		lblNombre.setFont(new Font("Segoe Print", Font.PLAIN, 22));
		frame.getContentPane().add(lblNombre);
		
		lNombreJugador = new JLabel("Jugador1");
		lNombreJugador.setBackground(Color.BLACK);
		lNombreJugador.setForeground(Color.CYAN);
		lNombreJugador.setBounds(130, 18, 213, 39);
		lNombreJugador.setFocusable(false);
		lNombreJugador.setFont(new Font("Segoe Print", Font.PLAIN, 20));
		frame.getContentPane().add(lNombreJugador);
		
		
		table = new JTable();
		table.setForeground(Color.WHITE);
		table.setFont(new Font("Segoe Print", Font.BOLD, 36));
		table.setBackground(Color.BLACK);
		table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(255, 255, 255)));
		table.setRowSelectionAllowed(false);
		table.setEnabled(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		table.setBounds(130, 102, 426, 335);
		table.setRowHeight(84);
		frame.getContentPane().add(table);
	}
	
	
	
	private void dibujarMatriz()
	{
		for(int i=0; i<4;i++)
		{
			for(int j=0;j<4;j++)
			{
				if(juego.getValor(i, j)!=0)
				{
					String data=juego.getValor(i, j).toString();
					if(juego.getValor(i, j)<10)
						table.setValueAt("   "+data, i, j);
					else if(juego.getValor(i, j)<100)
						table.setValueAt("  "+data, i, j);
					else if(juego.getValor(i, j)<1000)
						table.setValueAt(" "+data, i, j);
					else
						table.setValueAt(""+data, i, j);
				}
				else
				{	
					table.setValueAt(null, i, j);
				}
			}
		}
	}
	
	public void guardarPuntaje()
	{
		if(this.seGuardoPuntaje==false && juego.getTerminoJuego()==true)
		{
			String nom=lNombreJugador.getText();
			juego.almacenarPuntaje(nom, juego.getPuntaje());
			this.seGuardoPuntaje=true;
			JOptionPane.showMessageDialog(null, "�Lo siento te quedaste sin movimientos!"
					+ " Si tu puntaje esta entre los 10 primeros podras verlo en la tabla.");
			lNombreJugador.setFocusable(true);
		}
		if(juego.getHay2048()==true && hubo2048==false)
		{
			JOptionPane.showMessageDialog(null, "�LLegaste a 2048 felicidades!"
					+ " Podes seguir incrementando tu puntaje hasta quedarte sin movimientos, suerte!");
			hubo2048=true;
		}
	}
}
