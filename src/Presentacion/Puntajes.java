package Presentacion;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTable;
import Logica.Juego;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Puntajes {

	private JFrame frmPuntajes;
	private JTable table;
	private Juego juego = new Juego();

	/**
	 * Launch the application.
	 */
	public void mostrarPuntajes() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Puntajes window = new Puntajes();
					window.frmPuntajes.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Puntajes() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPuntajes = new JFrame();
		frmPuntajes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_1)
					System.out.println("hola");
			}
		});
		frmPuntajes.getContentPane().setBackground(Color.BLACK);
		frmPuntajes.setTitle("Puntajes");
		frmPuntajes.setBounds(600, 200, 595, 587);
		frmPuntajes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		String[] columnNames = { "","" };
		juego.traerPuntajes();
				frmPuntajes.getContentPane().setLayout(null);
		
				table = new JTable(juego.getPuntajes(), columnNames);
				table.setShowVerticalLines(false);
				table.setShowHorizontalLines(false);
				table.setBounds(137, 144, 431, 313);
				frmPuntajes.getContentPane().add(table);
				table.setForeground(Color.GREEN);
				table.setFont(new Font("Segoe Print", Font.BOLD, 18));
				table.setBackground(Color.BLACK);
				table.setRowHeight(32);
				table.setEnabled(false);
				
				JLabel lblNewLabel = new JLabel("Top 10");
				lblNewLabel.setFont(new Font("Vineta BT", Font.PLAIN, 29));
				lblNewLabel.setForeground(Color.GREEN);
				lblNewLabel.setBounds(211, 52, 167, 40);
				frmPuntajes.getContentPane().add(lblNewLabel);

	}
}
