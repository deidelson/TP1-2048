package Logica;

import java.util.Random;

public class Matriz 
{
	private TuplaInt [][] matrizJuego;
	
	
	public Matriz()
	{
		matrizJuego=new TuplaInt[4][4];
		for(int i=0; i<matrizJuego.length;i++)
		{
			for(int j=0; j<matrizJuego[i].length;j++)
			{
				matrizJuego[i][j]=new TuplaInt();
			}
		}
	}
	
	public int largo()
	{
		return this.matrizJuego.length;
	}
	
	public TuplaInt[][] getMatriz()
	{
		return this.matrizJuego;
	}
	
	public boolean equals(Matriz m)
	{
		boolean ret=true;
		for(int i=0; i<this.largo();i++)
		{
			for(int j=0; j<this.largo(); j++)
			{
				ret=ret && this.getValor(i, j).equals(m.getValor(i, j));
			}
		}
		return ret;
	}
	
	public void copiarMatriz(Matriz m)
	{
		for(int i=0; i<this.largo();i++)
		{
			for(int j=0; j<this.largo(); j++)
			{
				this.setValor(m.getValor(i, j), i, j);
			}
		}
	}
	
	public void setValor(Integer valor, int i, int j)
	{
		this.matrizJuego[i][j].setNumero(valor);
	}
	
	
	
	public void borrarValor(int i, int j)
	{
		this.matrizJuego[i][j].borrarInformacion();
	}
	
	public Integer getValor(int i, int j)
	{
		return matrizJuego[i][j].getNumero();
	}
	
	public boolean getSePuedeCombinar(int i, int j)
	{
		return matrizJuego[i][j].getPuedeCombinar();
	}
	
	public void setSePuedeCombinar(boolean estado, int i, int j)
	{
		matrizJuego[i][j].setPuedeCombinar(estado);
	}
	
	public void todosPuedenCombinar()
	{
		for(int i=0; i<matrizJuego.length;i++)
		{
			for(int j=0; j<matrizJuego[i].length;j++)
			{
				matrizJuego[i][j].setPuedeCombinar(true);
			}
		}
	}
	
	public void moverDatos(int desdeI, int desdeJ, int hastaI, int hastaJ)
	{
		this.setValor(this.getValor(desdeI, desdeJ), hastaI, hastaJ);
		this.setSePuedeCombinar(this.getSePuedeCombinar(desdeI, desdeJ), hastaI, hastaJ);
		this.borrarValor(desdeI, desdeJ);
	}
	
	public void moverYSumarValor(int desdeI, int desdeJ, int hastaI, int hastaJ)
	{
		int suma=this.getValor(desdeI, desdeJ)+this.getValor(hastaI, hastaJ);
		this.setValor(suma, hastaI, hastaJ);
		this.setSePuedeCombinar(false, hastaI, hastaJ);
		this.borrarValor(desdeI, desdeJ);
	}
	
	public void generarNumeroAlAzar()
	{
		if(this.verificarSiEstaLlena()==false)
		{
			
			Random r=new Random();
			Integer dato= r.nextInt(2);
			dato=(dato+1)*2;
			
			int coordenadaI=r.nextInt(4);
			int coordenadaJ=r.nextInt(4);
			
			
			while(getValor(coordenadaI,coordenadaJ)!=0)
			{
				coordenadaI=r.nextInt(4);
				coordenadaJ=r.nextInt(4);
			}
			
			this.setValor(dato, coordenadaI, coordenadaJ);
		}
	}
	
	public boolean verificarSiEstaLlena()
	{
		boolean ret=true;
		
		for (int i=0; i<this.matrizJuego.length;i++)
		{
			for(int j=0; j<this.matrizJuego[i].length;j++)
			{
				ret=ret && this.getValor(i, j)!=0;
			}
				
		}
		
		return ret;
	}

}
