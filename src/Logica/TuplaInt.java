package Logica;

public class TuplaInt {
	private Integer numero;
	private boolean puedeCombinar=true;
	
	public TuplaInt(Integer n)
	{
		this.numero=n;
		this.puedeCombinar=true;
	}
	
	public TuplaInt()
	{
		this.numero=0;
		this.puedeCombinar=true;
	}
	
	public int getNumero()
	{
		return this.numero;
	}
	
	public void setNumero(int n)
	{
		this.numero=n;
		
	}
	
	public boolean getPuedeCombinar()
	{
		return this.puedeCombinar;
	}
	
	public void setPuedeCombinar(boolean e)
	{
		this.puedeCombinar=e;
	}
	
	public void borrarInformacion()
	{
		this.numero=0;
		this.puedeCombinar=true;
	}

}
