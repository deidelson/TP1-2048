package Logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Datos.Conexion;

public class Juego
{
	private Matriz matriz=new Matriz();
	private Integer puntaje;
	private boolean terminoJuego;
	private boolean hay2048;
	private Object [][] datosBdPuntajes ;
	private Conexion conexion=new Conexion();
	private Connection cn=conexion.Conectar();
	
	
	public Juego()
	{
		this.puntaje=0;
		this.terminoJuego=false;
		this.matriz.generarNumeroAlAzar();
		this.matriz.generarNumeroAlAzar();
		this.datosBdPuntajes=new Object[10][2];
		this.hay2048=false;
	}
	
	public Object[][] getPuntajes()
	{
		return this.datosBdPuntajes;
	}
	
	public Integer getValor(int i, int j)
	{
		return this.matriz.getValor(i, j);
	}
	
	public Integer getPuntaje()
	{
		return this.puntaje;
	}
	
	public boolean getTerminoJuego()
	{
		return this.terminoJuego;
	}
	
	public boolean getHay2048()
	{
		return this.hay2048;
	}
	
	public TuplaInt[][] getMatriz()
	{
		return this.matriz.getMatriz();
	}
	
	public void verificarSiHay2048()
	{
		for(int i=0; i<this.matriz.largo();i++)
		{
			for(int j=0; j<this.matriz.largo();j++)
			{
				this.hay2048=this.hay2048 || this.matriz.getValor(i, j).equals(2048);
			}
		}
	}
	
	public void verificarSiterminoJuego()
	{
		boolean hayMovimientos=false;
		if(this.matriz.verificarSiEstaLlena()==true)
		{
			for(int i=0; i<this.matriz.largo();i++)
			{
				for(int j=0; j<this.matriz.largo();j++)
				{
					int arriba=i-1;
					int abajo=i+1;
					int izquierda=j-1;
					int derecha=j+1;
					
					if(arriba>=0 )
						hayMovimientos=hayMovimientos||matriz.getValor(i, j)==matriz.getValor(arriba, j);
					if(abajo<=3)
						hayMovimientos=hayMovimientos||matriz.getValor(i, j)==matriz.getValor(abajo, j);
					if(izquierda>=0)
						hayMovimientos=hayMovimientos||matriz.getValor(i, j)==matriz.getValor(i, izquierda);
					if(derecha<=3)
						hayMovimientos=hayMovimientos||matriz.getValor(i, j)==matriz.getValor(i, derecha);
				}
			}
			this.terminoJuego= !hayMovimientos;
		}
		
	}
	
	public void traerPuntajes()
	{
		try 
		{
			Statement st=cn.createStatement();
			ResultSet rs=st.executeQuery("SELECT Nombre, Puntaje FROM puntajes ORDER BY Puntaje desc");
			int i=0;int j=0;
			
			while(rs.next())
			{
				String nombreJug=rs.getString("Nombre");
				String puntajeJugador=rs.getString("Puntaje");
				Integer pj=Integer.parseInt(puntajeJugador);
				if(i<10)
				{
					datosBdPuntajes[i][j]=nombreJug;
					j++;
					datosBdPuntajes[i][j]=pj;
					i++;
					j=0;
				}
			}
		}
		catch (SQLException e) 
		{
			System.err.println(e.getMessage());
		}
	}
	
	public void almacenarPuntaje(String nombre, int puntaje)
	{
		try 
		{
			Statement st=cn.createStatement();
			st.executeUpdate("INSERT INTO puntajes(Nombre, Puntaje) VALUES (('"+nombre+"',"+puntaje+"))");
		} catch (SQLException e) 
		{
			System.err.println(e.getMessage());
		}
	}
	
	public void mover(String direccion)
	{
		Matriz copia=new Matriz();
		copia.copiarMatriz(this.matriz);
		for(int i=0; i<matriz.largo();i++)
		{
			if(direccion.equals("izquierda"))
				this.moverFilaIzquierda(i);
			if(direccion.equals("derecha"))
				this.moverFilaDerecha(i);
			if(direccion.equals("arriba"))
				this.moverColumnaArriba(i);
			if(direccion.equals("abajo"))
				this.moverColumnaAbajo(i);
		}
		matriz.todosPuedenCombinar();
		
		if(this.matriz.equals(copia)==false)
			matriz.generarNumeroAlAzar();
	}
	
	private void moverFilaIzquierda(int i)
	{
		for(int j=1;j<this.matriz.largo();j++)
		{
			int pivot=j;
			
			while(pivot>0)
			{
				if(matriz.getValor(i, pivot-1)==0)
				{
					matriz.moverDatos(i, pivot, i, pivot-1);
				}
				else if(matriz.getValor(i, pivot-1).equals(matriz.getValor(i, pivot)) && matriz.getSePuedeCombinar(i, pivot-1)==true
						&&  matriz.getSePuedeCombinar(i, pivot)==true)
				{
					matriz.moverYSumarValor(i, pivot, i, pivot-1);
					this.puntaje+=matriz.getValor(i, pivot-1);
				}
				pivot--;
			}
		}
	}
	
	private void moverFilaDerecha(int i) 
	{
		for(int j=matriz.largo()-2;j>=0;j--)
		{
			int pivot=j;
			
			while(pivot<matriz.largo()-1)
			{
				if(matriz.getValor(i, pivot+1)==0)
				{
					matriz.moverDatos(i, pivot, i, pivot+1);
				}
				else if(matriz.getValor(i, pivot+1).equals(matriz.getValor(i, pivot)) && matriz.getSePuedeCombinar(i, pivot+1)==true
						&&  matriz.getSePuedeCombinar(i, pivot)==true)
				{
					matriz.moverYSumarValor(i, pivot, i, pivot+1);
					this.puntaje+=matriz.getValor(i, pivot+1);
				}
				pivot++;
			}
		}
	}
	
	private void moverColumnaArriba(int i) 
	{
		for(int j=1;j<this.matriz.largo();j++)
		{
			int pivot=j;
			
			while(pivot>0)
			{
				if(matriz.getValor(pivot-1, i)==0)
				{
					matriz.moverDatos(pivot, i, pivot-1, i);
				}
				else if(matriz.getValor(pivot-1, i).equals(matriz.getValor(pivot, i)) && matriz.getSePuedeCombinar(pivot-1, i)==true
						&&  matriz.getSePuedeCombinar(pivot, i)==true)
				{
					matriz.moverYSumarValor(pivot, i, pivot-1, i);
					this.puntaje+=matriz.getValor(pivot-1, i);
				}
				pivot--;
			}
		}
	}
	
	private void moverColumnaAbajo(int i)
	{
		for(int j=matriz.largo()-2;j>=0;j--)
		{
			int pivot=j;
			
			while(pivot<matriz.largo()-1)
			{
				if(matriz.getValor(pivot+1, i)==0)
				{
					matriz.moverDatos(pivot, i, pivot+1, i);
				}
				else if(matriz.getValor(pivot+1, i).equals(matriz.getValor(pivot, i)) && matriz.getSePuedeCombinar(pivot+1, i)==true
						&&  matriz.getSePuedeCombinar(pivot, i)==true)
				{
					matriz.moverYSumarValor(pivot, i, pivot+1, i);
					this.puntaje+=matriz.getValor(pivot+1, i);
				}
				pivot++;
			}
		}
	}
}
